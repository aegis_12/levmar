
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "least_squares.h"

__cudaFunction__
void printVector(real *v, int n)
{
	int i;

	for (i=0; i<n; i++)
		printf("%f ", v[i]);

	printf("\n");
}

__cudaFunction__
void printMatrix(real *v, int m, int n)
{
	int i, j;
	for (i=0; i<m; i++){
		for (j=0; j<n; j++){
			printf("%f ", v[i+m*j]);
		}
		printf("\n");
	}
}

/* 
* Modificar para evitar overflows 
* Por ahora es una version simple. en las pruebas no ira
* muy lejos con los valores
*/
__cudaFunction__
real norm(real *v, int n)
{
	real res = 0.0; 
	int i = 0;

	for (i=0; i<n; i++)
		res += v[i] * v[i];

	return sqrt(res);
}

/**
* Devuelve beta y se sobreescribe el vector v
* x es una de las columnas de la matriz. N es el tamaño
* de ese 'vector'
*/
__cudaFunction__
real householder_vector(real *v, real *xA, int n) 
{
	
	int i, vLocal;
	real beta = 0.0, alpha=0, uta=0, xAux;

	real xNorm = norm(xA, n);

	v[0] = 1;

	real x[n];

	// Evitar overflow en x
	for (i=0; i<n; i++)
		x[i] = xA[i] / xNorm;

	/* Calcular el valor de alpha x(2:n)' * x(2:n) */
	for (i=1; i<n; i++)
	{
		// Crear alpha
		alpha += x[i] * x[i];

		// Iniciar el vector v
		v[i] = x[i];
	}

	if (alpha == 0){
		return 0;
	}
	else {
		uta = sqrt(x[0]*x[0] + alpha);

		if (x[0] <= 0){
			v[0] = x[0] - uta;
		} 
		else {
			v[0] = -alpha / (x[0]+uta);
		}

		beta = 2 * (v[0]*v[0]) / (alpha + v[0]*v[0]);
		xAux = v[0];

		/* Update v value */
		for (i=0; i<n; i++)
			v[i] = v[i] / xAux;
	}

	return beta;
}

__cudaFunction__
void qrSolver(real *B, real *A, int m, int n, real *step) 
{
	int i, j, c, d, k;

	real aux, beta;
	real v[m], sum;

	// se ira sobreescribiendo en cada iteracion
	// en la primera iteracion se usa todo el vector y el primero se cambia a uno
	// en las siguiente iteracion es el segunod elemento el que cambia a uno

	real mMult[m*m];

	// iniciar el array v
	for (i=0; i<m; i++)
		v[0] = 0.0;

	// Matriz de multiplacion
	for (i=0; i<m; i++)
		for (j=0; j<m; j++)
			mMult[i*m+j] = 0;

	// Hacer la copia sobre B con la matriz de A
	// la copia te que ser completa
	for (i=0; i<(m)*(n); i++)
	{
		A[i] = B[i];
	}

	for (i=0; i<n; i++)	// n
	{

		beta = householder_vector(v, &(A[(i*m)+i]), m-i);
		
		//
		// I - b * (v * v') en mMult
		// y empiezas desde mMult[i->:, i->:]
		//

		for (j=0; j<m-i; j++)
		{
			for (k=0; k<m-i; k++)
			{
				mMult[(j+i)+m*(k+i)] = -1 * beta * v[j] * v[k];
			}
		}

		//
		// Cambiar la diagonal (combinar en la de dalt?)
		//

		for (j=i; j<m; j++)
		{
			mMult[j*m+j] = 1 + mMult[j*m+j];
		}
		
		//
		// mMult * A en los rangos especificos
		//

		sum = 0;

		for (d = i; d < n; d++) {
			for (c = i; c < m; c++) {
				v[c] = 0;
	        	for (k = i; k < m; k++) 
	        	{
	        		v[c] = v[c] + mMult[c*m+k]*A[k+m*d];
	        	}
	      	}
	      	for (c=i; c<m; c++)
	      		A[c+m*d] = v[c];
	    }

	}

	// backsustitution

	int rows = n-1;

	// Ultimo valor
	step[rows-1] = A[rows*m+(rows-1)] / A[(rows-1)*m+(rows-1)];

	for (i=rows-2; i>=0; i--)
	{
		step[i] = A[i+m*rows];

		for (j=i+1; j<rows; j++)
		{
			step[i] -= A[j*m + i] * step[j];
		}
		step[i] = step[i] / A[i+m*i];
	}

}

typedef struct  
{
    real *ydata;
    real *xdata;
} 
fcndata_t;

__cudaFunction__
typedef void (*func_decl)(void *data, int nObs, int nPrm, real *x, real *fvec, real *fjac, int flag);

/**
*	Numero de incógnitas del modelo
*/

const int nPrm = 2;

/**
*	Definición del modelo (residuales y jacobianos)
*/

__cudaFunction__
void modeloIvim(void *data, int nObs, int nPrm, real *x, real *fvec, real *fjac, int flag) 
{
	int i;

    real *ydata = ((fcndata_t*)data)->ydata;
    real *xdata = ((fcndata_t*)data)->xdata;

	if (flag == JACOBIAN)
	{
		for (i = 0; i < nObs; i++) 
		{
			fjac[i]		 	  = -xdata[i];
			fjac[i+nObs+nPrm] =  xdata[i];
		}
	}

	if (flag == RESIDUAL)
	{
		for (i = 0; i < nObs; i++) 
		{
			fvec[i] = ydata[i] - (1 - x[0] * xdata[i] + x[1] * xdata[i] + xdata[i]);
		}
	}
}

__cudaFunction__
real measureSSE(real *x, int nObs)
{
	real rslt=0;
	int i;

	for (i=0; i<nObs; i++)
		rslt += x[i] * x[i];

	return rslt;
}

__cudaFunction__
void fitDiagonal(real *diag, real *fjac, real lambda, int nObs, int nPrm)
{
	int i;

	for (i=0; i<nPrm; i++)
	{
		fjac[(i*(nObs+nPrm))+nObs+(i)] = sqrt(lambda * diag[i]);
	}
}

__cudaFunction__
void computeJacobianDiagonal(real *diag, real *fjac, int nObs, int nPrm)
{
	int i, j;
	real sum = 0.0, val = 0.0;

	for (i=0; i<nPrm; i++)
	{
		sum = 0.0;

		for (j=0; j<nObs; j++)
		{
			val = fjac[i*(nObs+nPrm)+j];
			sum += val * val;
		}
		diag[i] = sum;
	}
}

__cudaFunction__
void updateValue(real *dest, real *source, real *step, int nObs)
{
	int i;

	for (i=0; i<nObs; i++)
		dest[i] = source[i] + step[i];
}

real ComputeSseFromModel(func_decl model, void *data, int nObs, int nPrm, real *x, real *fvec)
{
	/**
	*	En fvec se almacena el residual
	*/
	model(data, nObs, nPrm, x, fvec, 0, RESIDUAL);

	return measureSSE(fvec, nObs);
}

__cudaFunction__
int least_squares(func_decl model, void *data, real *x);

int main()
{

	real x[nPrm];

	real xDataLocal[5] = {32.0, 67.0, 53.0, 12.0, 23.0};
    real yDataLocal[5] = {12.0, 23.0, 34.0, 54.0, 32.0};

	fcndata_t data;
	data.xdata = xDataLocal;
	data.ydata = yDataLocal;

	x[0] = 0.3;
	x[1] = 0.8;

	int sol = least_squares(&modeloIvim, &data, x);

}

__cudaFunction__
int least_squares(func_decl model, void *data, real *x)
{
	
	printf("%f %f\n", x[0], x[1]);
	// 
	// Valores para la salida
	// 

    int EndCode  = ENDCODE_MAXITER;

	int nObs = 5;

	int i;

	real lambda = 0.01;

	real diagJ[nPrm];

	// double fvec[nObs];
	real fjac[(nObs+nPrm)*(nPrm+1)];

	// tot menos el ultim del param
	real B[(nObs+nPrm)*(nPrm+1)];

	// real x[nPrm];	// iniciar X
	real xOld[nPrm];

	real step[nPrm];  // el cambio del linear solver

	// x[0] = 0.1;
	// x[1] = 0.1;

	real sse = 0;
	real sseOld = 0;

	int iter = 0;
	int maxiter = 100;

	real *fvec = &(fjac[nPrm*(nObs+nPrm)]);

	// (func_decl model, void *data, int nObs, int nPrm, real *x, real *fvec)
	sse = ComputeSseFromModel(model, data, nObs, nPrm, x, fvec);

	//model(data, nObs, nPrm, x, fvec, fjac, RESIDUAL);
	//sse = measureSSE(fvec, nObs);

	real rtol 	 = 1.0000e-08;
	real eps 	 = 2.2204e-16;
	real betatol = 1.0000e-08;
	real sqrteps = 1.4901e-08;


	while (iter < maxiter && EndCode == ENDCODE_MAXITER)
	{
		iter = iter + 1;
		sseOld = sse;

		// xOld = x
		for (i=0; i<nPrm; i++)
			xOld[i] = x[i];
		
		printf("%f %f\n", x[0],x[1]);
		// jacobiano
		model(data, nObs, nPrm, x, fvec, fjac, JACOBIAN);
		printf("%f %f\n", x[0],x[1]);

		computeJacobianDiagonal(diagJ, fjac, nObs, nPrm);

		fitDiagonal(diagJ, fjac, lambda, nObs, nPrm);
		qrSolver(fjac, B, nObs+nPrm, nPrm+1, step);

		// 
		// x = x + step (no funciona con updateValue)
		// 

		for (i=0; i<nPrm; i++)
			x[i] = x[i] + step[i];

		// model(data, nObs, nPrm, x, fvec, fjac, RESIDUAL);
		// sse = measureSSE(fvec, nObs);

		sse = ComputeSseFromModel(model, data, nObs, nPrm, x, fvec);

		if (sse < sseOld)
		{
			lambda = max(0.1*lambda, eps);
		}
		else
		{
			while (sse > sseOld)
			{
				lambda = 10*lambda;
				if (lambda > 1e16)
				{
					EndCode = ENDCODE_STALL;
				}

				fitDiagonal(diagJ, fjac, lambda, nObs, nPrm);
				qrSolver(fjac, B, nObs+nPrm, nPrm+1, step);

				// actualizar beta()
				updateValue(x, xOld, step, nObs);

				// model(data, nObs, nPrm, x, fvec, fjac, RESIDUAL);
				// sse = measureSSE(fvec, nObs);

				sse = ComputeSseFromModel(model, data, nObs, nPrm, x, fvec);

			}
		}

		if (abs(sse-sseOld) <= rtol*sse)
		{
			EndCode = ENDCODE_TOLFUN;
		}
		else if (norm(step, nPrm) < betatol*(sqrteps+norm(x, nObs)))
		{
			EndCode = ENDCODE_TOLX;
		}

	}

	printf("Numero de iteraciones %d\n", iter);
	printf("---VECTOR FINAL---\n");
	printVector(x, nPrm);

	return EndCode;
}