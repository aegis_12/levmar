
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if defined(__CUDA_ARCH__) || defined(__CUDACC__)
#define __cudaFunction__ __device__
#ifndef __real__
#define __real__ float
#endif
#else
#define __cudaFunction__
#ifndef __real__
#define __real__ double
#endif
#endif

#define RESIDUAL 1
#define JACOBIAN 2

#define real __real__

#define min(a,b) ((a) <= (b) ? (a) : (b))
#define max(a,b) ((a) >= (b) ? (a) : (b))
#define TRUE (1)
#define FALSE (0)

#define ENDCODE_MAXITER 0
#define ENDCODE_STALL   1
#define ENDCODE_TOLFUN  2
#define ENDCODE_TOLX	3

#ifdef __cplusplus
}
#endif /* __cplusplus */